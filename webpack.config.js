/**
 * Created by jiangyangjun@baixing.com on 16/7/12.
 */
var ExtractTextPlugin = require('extract-text-webpack-plugin')

var AUTOPREFIXER_BROWSERS = [
	'Android 2.3',
	'Android >= 4',
	'Chrome >= 20',
	'Firefox >= 24',
	'Explorer >= 8',
	'iOS >= 6',
	'Opera >= 12',
	'Safari >= 6'
]

module.exports = {
	entry: {
		'bx-antd': './src/index.js',
		'bx-antd.custom': './src/index.custom.js',
	},

	output: {
		path: './dist',
		filename: '[name].js',
		libraryTarget: 'var',
		library: 'antd',
	},

	resolve: {
		extensions: ['', '.js']
	},

	devtool: '#source-map',

	module: {
		loaders: [{
			test: /\.less$/,
			loader: ExtractTextPlugin.extract('css?sourceMap&-minimize!' + 'postcss-loader!' + 'less?sourceMap')
		}]
	},

	externals: {
		'react': 'React',
		'react-dom': 'ReactDOM'
	},

	postcss: function () {
		return [
			require('autoprefixer')({ browsers: AUTOPREFIXER_BROWSERS }),
		]
	},

	plugins: [
		new ExtractTextPlugin("[name].css"),
	]
}
